/**
 * @format
 */

import {AppRegistry} from 'react-native';
import AppScreen from './src/screens/app-screen';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AppScreen);
