import {createDrawerNavigator} from '@react-navigation/drawer';
import BottomTabNavigator from './bottom-tab-navigator';
import React from 'react';
import OpenDrawerScreen from '../screens/open-drawer-screen';

const Drawer = createDrawerNavigator();

export default function DrawerNavigator() {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={BottomTabNavigator} />
    </Drawer.Navigator>
  );
}
