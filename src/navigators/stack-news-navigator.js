import React, {useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import NewsListScreen from '../screens/news/news-list-screen';
import NewsDetailsScreen from '../screens/news/news-details-screen';
import {LocalizationContext} from '../i18n';

const Stack = createStackNavigator();

export default function StackNewsNavigator() {
  const {t} = useContext(LocalizationContext);

  return (
    <Stack.Navigator>
      <Stack.Screen name={t('newsList')} component={NewsListScreen} />
      <Stack.Screen name={t('newsDetails')} component={NewsDetailsScreen} />
    </Stack.Navigator>
  );
}
