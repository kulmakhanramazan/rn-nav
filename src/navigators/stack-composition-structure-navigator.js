import React, {useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import CommitteesListScreen from '../screens/comsposition-structure/committees-list-screen';
import CommitteesDetailsScreen from '../screens/comsposition-structure/committees-details-screen';
import {LocalizationContext} from '../i18n';
import CompositionStructureTabs from '../screens/comsposition-structure/composition-structure-tabs';
import DeputiesListScreen from '../screens/comsposition-structure/deputies-list-screen';
import DeputiesDetailsScreen from '../screens/comsposition-structure/deputies-details-screen';

const Stack = createStackNavigator();

export default function StackCompositionStructureNavigator() {
  const {t} = useContext(LocalizationContext);

  return (
    <Stack.Navigator inititalRoute={t('compositionStructure')}>
      <Stack.Screen
        name={t('compositionStructure')}
        component={CompositionStructureTabs}
        options={{headerShown: false}}
      />
      <Stack.Screen name={t('deputiesList')} component={DeputiesListScreen} />
      <Stack.Screen
        name={t('deputyDetails')}
        component={DeputiesDetailsScreen}
      />
      <Stack.Screen
        name={t('committeeList')}
        component={CommitteesListScreen}
      />
      <Stack.Screen
        name={t('committeeDetails')}
        component={CommitteesDetailsScreen}
      />
    </Stack.Navigator>
  );
}
