import React, {useContext} from 'react';
import {Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Контекст локализации
import {LocalizationContext} from '../i18n';
import StackNewsNavigator from './stack-news-navigator';
import StackCompositionStructureNavigator from './stack-composition-structure-navigator';
import OpenDrawerScreen from '../screens/open-drawer-screen';
const BottomTab = createBottomTabNavigator();

export default function BottomTabNavigator() {
  const {t} = useContext(LocalizationContext);

  return (
    <BottomTab.Navigator
      initialRouteName={t('news')}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let pictureName = require('../assets/icons/news.png');
          if (route.name === t('news') && focused === false) {
            pictureName = require('../assets/icons/news.png');
          } else if (route.name === t('news') && focused === true) {
            pictureName = require('../assets/icons/news-focused.png');
          } else if (
            route.name === t('compositionStructure') &&
            focused === false
          ) {
            pictureName = require('../assets/icons/structure.png');
          } else if (
            route.name === t('compositionStructure') &&
            focused === true
          ) {
            pictureName = require('../assets/icons/structure-focused.png');
          }
          return <Image source={pictureName} />;
        },
      })}>
      <BottomTab.Screen name={t('news')} component={StackNewsNavigator} />
      <BottomTab.Screen
        name={t('compositionStructure')}
        component={StackCompositionStructureNavigator}
      />
      <BottomTab.Screen name="openDrawer" component={OpenDrawerScreen} />
    </BottomTab.Navigator>
  );
}
