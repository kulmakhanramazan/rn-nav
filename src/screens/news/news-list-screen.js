import React, {useContext} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Text, View, Button} from 'react-native';
import styles from '../../styles/styles';
import {LocalizationContext} from '../../i18n';

export default function NewsListScreen() {
  const navigation = useNavigation();
  const {t} = useContext(LocalizationContext);
  return (
    <View style={[styles.container, styles.textXYCenter]}>
      <Text style={styles.heading}>News List</Text>
      <Button
        title="Open news details"
        onPress={() => navigation.navigate(t('newsDetails'))}
      />
    </View>
  );
}
