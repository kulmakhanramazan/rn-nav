import 'react-native-gesture-handler';
import React, {useEffect, useState, useMemo} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {StatusBar} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import I18n, {LocalizationContext} from '../i18n';
import BottomTabNavigator from '../navigators/bottom-tab-navigator';
import DrawerNavigator from '../navigators/drawer-navigator';

export default function () {
  const [locale, setLocale] = useState('kk');
  useEffect(() => changeLocale(), [locale]);

  const changeLocale = () => {
    AsyncStorage.getItem('@locale_Key').then((value) => {
      if (value !== null) {
        setLocale(value);
      }
    });
  };

  const localizationContext = useMemo(
    () => ({
      t: (scope, options) => I18n.t(scope, {locale, ...options}),
      locale,
      setLocale,
    }),
    [locale],
  );

  return (
    <LocalizationContext.Provider value={localizationContext}>
      <NavigationContainer>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <DrawerNavigator />
      </NavigationContainer>
    </LocalizationContext.Provider>
  );
}
