import React, {useContext} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Text, View, Button} from 'react-native';
import styles from '../styles/styles';
import {LocalizationContext} from '../i18n';

export default function OpenDrawerScreen() {
  const navigation = useNavigation();
  const {t} = useContext(LocalizationContext);
  return (
    <View style={[styles.container, styles.textXYCenter]}>
      <Button
        title="Open drawer menu"
        onPress={() => navigation.toggleDrawer()}
      />
    </View>
  );
}
