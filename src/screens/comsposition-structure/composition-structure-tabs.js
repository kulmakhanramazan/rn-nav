import React from 'react';
import {Dimensions} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import DeputiesListScreen from './deputies-list-screen';
import CommitteesListScreen from './committees-list-screen';
import {LocalizationContext} from '../../i18n';

const initialLayout = {width: Dimensions.get('window').width};

export default function CompositionStructureTabs() {
  // const {t} = React.useContext(LocalizationContext);
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Депутаты'},
    {key: 'second', title: 'Комитеты'},
  ]);

  const renderScene = SceneMap({
    first: DeputiesListScreen,
    second: CommitteesListScreen,
  });

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      bounces
      indicatorStyle={{backgroundColor: 'red'}}
      style={{backgroundColor: 'white'}}
      labelStyle={{color: 'black'}}
    />
  );

  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
  );
}
