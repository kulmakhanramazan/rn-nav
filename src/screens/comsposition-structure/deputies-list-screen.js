import React, {useContext} from 'react';
import {Text, View, Button} from 'react-native';
import styles from '../../styles/styles';
import {useNavigation} from '@react-navigation/native';
import {LocalizationContext} from '../../i18n';

export default function DeputiesListScreen() {
  const navigation = useNavigation();
  const {t} = useContext(LocalizationContext);
  return (
    <View style={[styles.container, styles.textXYCenter]}>
      <Text style={styles.heading}>Deputies List</Text>
      <Button
        title="Open deputy details"
        onPress={() => navigation.navigate(t('deputyDetails'))}
      />
    </View>
  );
}
