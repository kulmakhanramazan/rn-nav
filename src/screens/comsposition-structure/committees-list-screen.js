import React, {useContext} from 'react';
import {Text, View, Button} from 'react-native';
import styles from '../../styles/styles';
import {useNavigation} from '@react-navigation/native';
import {LocalizationContext} from '../../i18n';

export default function CommitteesListScreen() {
  const navigation = useNavigation();
  const {t} = useContext(LocalizationContext);
  return (
    <View style={[styles.container, styles.textXYCenter]}>
      <Text style={styles.heading}>Committees List</Text>
      <Button
        title="Open committee details"
        onPress={() => navigation.navigate(t('committeeDetails'))}
      />
    </View>
  );
}
