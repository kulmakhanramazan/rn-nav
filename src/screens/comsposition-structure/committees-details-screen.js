import React from 'react';
import {Text, View} from 'react-native';
import styles from '../../styles/styles';

export default function CommitteesDetailsScreen() {
  return (
    <View style={[styles.container, styles.textXYCenter]}>
      <Text style={styles.heading}>Committee Details</Text>
    </View>
  );
}
