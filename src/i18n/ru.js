export default {
  news: 'Новости',
  activity: 'Деятельность',
  compositionStructure: 'Состав и структура',
  newsList: 'Список новостей',
  newsDetails: 'Новости детально',
  committeeList: 'Список комитетов',
  committeeDetails: 'Комитет детально',
  deputiesList: 'Список депутатов',
  deputyDetails: 'Депутат детально',
};
