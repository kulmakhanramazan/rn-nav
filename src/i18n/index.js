import I18n from 'react-native-i18n';
import React from 'react';

import ru from './ru';
import kk from './kk';
import en from './en';

I18n.fallbacks = true;

I18n.translations = {
  ru,
  kk,
  en,
};

I18n.defaultLocale = 'kk';
export default I18n;

/**
 * Подробно про хуки контекста
 * @link https://ru.reactjs.org/docs/context.html#reactcreatecontext
 *
 * Подробно про локализации в React Navigation
 * @link https://reactnavigation.org/docs/localization
 * */
export const LocalizationContext = React.createContext();
