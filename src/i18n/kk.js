export default {
  news: 'Жаңалықтар',
  activity: 'Қызметтер',
  compositionStructure: 'Құрамы және құрылысы',
  newsList: 'Жаңалықтар тізімі',
  newsDetails: 'Жаңалықтар толықтай',
  committeeList: 'Комиттетер тізімі',
  committeeDetails: 'Комиттеттер толықтай',
  deputiesList: 'Депутаттар тізімі',
  deputyDetails: 'Депутат толықтай',
};
