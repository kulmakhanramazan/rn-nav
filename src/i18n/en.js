export default {
  news: 'News',
  activity: 'Activity',
  compositionStructure: 'Composition and structure',
  newsList: 'List of news',
  newsDetails: 'News details',
  committeeList: 'List of committee',
  committeeDetails: 'Committee details',
  deputiesList: 'List of deputies',
  deputyDetails: 'deputy details',
};
